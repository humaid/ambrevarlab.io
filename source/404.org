#+TITLE: 404 - Page not found
#+HTML_HEAD_EXTRA: <link rel="stylesheet" type="text/css" href="dark.css">
#+HTML_HEAD_EXTRA: <link rel="icon" type="image/x-icon" href="logo.png">
#+HTML_HEAD_EXTRA: <link href="atom.xml" type="application/atom+xml" rel="alternate" title="Pierre Neidhardt's homepage">

Sorry, we couldn’t find the requested URL. You can try again by going [[../][back
to the homepage]].
