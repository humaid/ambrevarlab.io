#+TITLE: A blog in pure Org/Lisp
#+SUBTITLE: A pamphlet for hackable website systems
#+DATE: <2018-08-13 Mon>

* The importance of blogging

Blogs (or personal websites) are an essential piece of the the Internet as
means of sharing knowledge openly.  In particular, blogs really shine at
sharing:

- articles [[http://daniellakens.blogspot.com/2017/04/five-reasons-blog-posts-are-of-higher.html][(before landing in access-restricting journals)]],
- projects,
- literature reference and all other sources of knowledge.  It's a good place to
  keep track of [[https://ambrevar.xyz/links/index.html][web links of articles and videos]].

A [[https://en.wikipedia.org/wiki/Web_feed][web feed]] (for instance RSS or Atom) is important to free the visitors from
manually checking for updates: let the updates go to them.

* Nothing but Org

The World Wide Web was devised to use HTML, which is rather painful to write
directly.  I don't want to go through that, it's too heavy a burden.  Many web
writers including me until recently use the [[https://daringfireball.net/projects/markdown/][Markdown]] format.

Nonetheless, for a long time I've been wanting to write blog posts in the [[https://orgmode.org/][Org]]
format.  I believe that Org is a much superior markup format for reasons that
are already well laid down by [[http://karl-voit.at/2017/09/23/orgmode-as-markup-only][Karl Voit]].  I can't help but highlight a few more
perks of Org:

- It has excellent math support (see my [[../homogeneous/index.org][article on homogeneous coordinates]] for
  an example).  For an HTML output, several backends are supported including
  [[https://www.mathjax.org/][MathJax]].  It's smart enough not to include MathJax when there is no math.  To
  top it all, there is no extra or weird syntax: it's simply raw TeX / LaTeX.

- It supports file hierarchies and updates inter-file links dynamically.  It
  also detects broken links on export.

- It has excellent support for multiple export formats, including LaTeX and PDFs.

* Version control system

A significant downside of printed books and documents is that they can't be
updated.  That is to say, unless you acquire a new edition, should there be any.
The Internet comes with the advantage that it allows to update content
worldwide, in a fraction of an instant.

Updating is important: originals are hardly ever typo-free; speculations might
turn out to be wrong; phrasing could prove ambiguous.  And most importantly, the
readers feedback can significantly help improve the argumentation and need be
taken into account.

The general trend around blogging seems to go in the other direction: articles
are often published and left as-is, never to be edited.

As such, many blog articles are struck by the inescapable flail of time and
technological advancement: they run out of fashion and lose much of their
original value.

But there is a motivation behind this immobility: the ability to edit removes
the guarantee that readers can access the article in its original
form.  Content could be lost in the process.  External references become
meaningless if the content has been removed or changed from the source they
refer to.

Thankfully there is a solution to this problem: version control systems.  They
keep all versions available to the world and make editing fully transparent.

I keep the source of my website at
https://gitlab.com/ambrevar/ambrevar.gitlab.io, in a public [[https://git-scm.com/][Git]] repository.

I cannot stress enough the importance of [[../vcs/index.org][keeping your projects under version
control]] in a /publicly readable repository/:

- It allows not only you but also all visitors to keep track of /all/ changes.
  This gives a /guarantee of transparency/ to your readers.

- It makes it trivial for anyone to /clone/ the repository locally: the website
  can be read offline in the Org format!

* Publishing requirements

[[https://orgmode.org/worg/org-blog-wiki.html][Worg has a list of blogging systems]] that work with the Org format.  Most of them
did not cut it for me however because I think a website needs to meet
important requirements:

- Full control over the URL of the published posts. :: This is a golden rule of
     the web: should I change the publishing system, I want to be able to stick
     to the same URLs or else all external references would be broken.  This is
     a big no-no and in my opinion it makes most blogging systems unacceptable.

- Top-notch Org support. :: I believe generators like Jekyll and Nikola only
     have partial Org support.

- Simple publishing pipeline. :: I want the generation process to be as simple
     as possible.  This is important for maintenance.  Should I someday switch
     host, I want to be sure that I can set up the same pipeline.

- Full control over the publishing system. :: I want maximum control over the
     generation process.  I don't want to be restricted by a non-Turing-complete
     configuration file or a dumb programming language.

- Ease of use. :: The process as a whole must be as immediate and friction-less
                  as possible, or else I take the risk of feeling too lazy to
                  publish new posts and update the content.

- Hackability. :: Last but not least, and this probably supersedes all other
                  requirements: /The system must be hackable/.  Lisp-based
                  systems are prime contenders in that area.

* Org-publish

This narrows down the possibilities to just one, if I'm not mistaken: Emacs with
Org-publish.

- The [[https://gitlab.com/ambrevar/ambrevar.gitlab.io/blob/master/publish.el][configuration]] happens in Lisp which gives me maximum control.

- Org-support is obviously optimal.

- The pipeline is as simple as it gets:
  #+BEGIN_SRC sh
  emacs --quick --script publish.el --funcall=org-publish-all
  #+END_SRC

Org-publish comes with [[https://orgmode.org/manual/Publishing.html][lots of options]], including sitemap generation (here [[../articles.org][my
post list]] with anti-chronological sorting).  It supports code highlighting
through the =htmlize= package.

** Webfeeds

One thing it lacked for me however was the generation of web feeds (RSS or
Atom).  I looked at the existing possibilities in Emacs Lisp but I could not
find anything satisfying.  There is =ox-rss= in Org-contrib, but it only works
over a single Org file, which does not suit my needs of one file per blog post.
So I went ahead and implemented [[https://gitlab.com/ambrevar/emacs-webfeeder][my own generator]].

** History of changes (dates and logs)

Org-publish comes with a timestamp system that proves handy to avoid building
unchanged files twice.  It's not so useful though to retrieve the date of last
modification because a file may be rebuilt for external reasons (e.g. change in
the publishing script).

Since I use the version control system (here Git), it should be most natural to
keep track of the creation dates and last modification date of the article.

Org-publish does not provide direct support for Git, but thanks to Lisp this
feature can only be a simple hack away:

#+BEGIN_SRC elisp
(defun ambrevar/git-creation-date (file)
  "Return the first commit date of FILE.
Format is %Y-%m-%d."
  (with-temp-buffer
    (call-process "git" nil t nil "log" "--reverse" "--date=short" "--pretty=format:%cd" file)
    (goto-char (point-min))
    (buffer-substring-no-properties (line-beginning-position) (line-end-position))))

(defun ambrevar/git-last-update-date (file)
  "Return the last commit date of FILE.
Format is %Y-%m-%d."
  (with-output-to-string
    (with-current-buffer standard-output
      (call-process "git" nil t nil "log" "-1" "--date=short" "--pretty=format:%cd" file))))
#+END_SRC

Then only ~org-html-format-spec~ is left to hack so that the ~%d~ and ~%C~
specifiers (used by ~org-html-postamble-format~) rely on Git instead.

See [[https://gitlab.com/ambrevar/ambrevar.gitlab.io/blob/master/publish.el][my publishing script]] for the full implementation.

* Personal domain and HTTPS

I previously stressed out the importance of keeping the URL permanents.  Which
means that we should not rely on the domain offered by a hosting platform such
as [[https://about.gitlab.com/features/pages/][GitLab Pages]], since changing host implies changing domain, thus invalidating
all former post URLs.  Acquiring a domain is a necessary step.

This might turn off those looking for the cheapest option, but in fact getting
domain name comes close to zero cost if you are not limitating yourself to just a
subset of popular options.  For a personal blog, the domain name and the
top-level domain should not matter much and can be easily adjusted to bring the
costs to a minimum.

There are many registrars to choose from.  One of the biggest, GoDaddy has [[https://en.wikipedia.org/wiki/GoDaddy#Controversies][a
debatable reputation]].  I've opted for https://www.gandi.net/.

With a custom domain, we also need a certificate for HTTPS.  This used to come
at a price but is now free and straightforward with [[https://letsencrypt.org/][Let's Encrypt]].  Here is a
[[https://about.gitlab.com/2016/04/11/tutorial-securing-your-gitlab-pages-with-tls-and-letsencrypt/][tutorial for GitLab pages]].  (Note that the commandline tool is called [[https://certbot.eff.org][certbot]]
now.)

* Permanent URLs and folder organization pitfalls

[[https://nullprogram.com/blog/2017/09/01/][Chris Wellons]] has some interesting insights about the architecture of a blog.

[[https://www.w3.org/Provider/Style/URI][URLs are forever]], and as such a key requirement of every website is to ensure
all its URLs will remain permanent.  Thus the folder organization of the blog
has to be thought of beforehand.

- Keep the URLs human-readable and easy to remember. ::  Make them short and
     meaningful.

- Avoid dates in URLs. :: This is a very frequent mishappen with blogs.  There
     are usually no good reason to encode the date in the URL of a post, it only
     makes it harder to remember and more prone to change when moving platform.

- Avoid hierarchies. :: Hierarchies usually don't help with the above points,
     put everything under the same folder instead.  Even if some pages belong to
     different "categories" (for instance "articles" and "projects"), this is
     only a matter of presentation on the sitemap (or the welcome page).  It
     should not influence the URLs.  When the category is left out, it's one
     thing less to remember whether the page =foo= was an article or a project.

- Place =index.html= files in dedicated folders. :: If the page extension does
     not matter (e.g. between =.html= and =.htm=), you can easily save the
     visitors from any further guessing by storing your =foo= article in
     =foo/index.html=.  Thus browsing =https://domain.tld/foo/= will
     automatically retrieve the right document.  It's easier and shorter than
     =https://domain.tld/foo.htm=.

- Don't rename files. :: Think twice before naming a file: while you can later
     tweak some virtual mapping between the URL and a renamed file, it's better
     to stick to the initial names to keep the file-URL association as
     straightforward as possible.

* Future development

There are more features I'd like to add to my homepage.  Ideally, I'd rather
stick to free software, limit external resources (i.e. avoid depending on
external Javascript), keep code and data under my control.

Implementation suggestions and other ideas are more than welcome!

- Comment system. :: Disqus is non-free, [[https://github.com/phusion/juvia][Juvia]] looks good but it's unmaintained,
     [[http://tildehash.com/?page=hashover][hashover]] seems to be one of the nicest options.  I'm also particularly
     interested in the [[https://www.w3.org/TR/webmention/][Webmention]] W3C recommendation.  For now I simplly rely on
     the [[https://gitlab.com/ambrevar/ambrevar.gitlab.io/issues][GitLab issue page]].

- Statistics and analytics. :: [[https://matomo.org/][Matomo]] looks interesting.  I'd need to set it up
     on a server.

- Tags and ubiquitous fuzzy-search. :: I'd like to add a universal search bar
     with tag support so that the complete website can be fuzzy-searched and
     filtered by tags.

* Other publishing systems

- [[https://github.com/greghendershott/frog][Frog]] is a blog generator written in [[https://racket-lang.org/][Racket]].  While it may be one of the best
  of its kind, it sadly does not support the Org format as of this writing.
  Some blogs generated with Frog:
  - https://alex-hhh.github.io
  - http://jao.io/pages/about.html

- [[https://dthompson.us/projects/haunt.html][Haunt]] is a blog generator written in [[https://www.gnu.org/software/guile/][Guile]].  It seems to be very complete and
  extensible, but sadly it does not support the Org format as of this writing.
  Some blogs generated with Haunt:
  - https://dthompson.us/index.html
  - https://www.gnu.org/software/guix/

- [[https://github.com/kingcons/coleslaw][Coleslaw]] is a blog generator in [[https://common-lisp.net/][Common Lisp]].  It seems to be very complete and
  extensible, but sadly it does not support the Org format as of this writing.
  It has a commandline tool, [[https://github.com/40ants/coleslaw-cli][Coleslaw-cli]].  Some blogs generated with Coleslaw:
  - https://github.com/kingcons/coleslaw/wiki/Blogroll

* Other Org-blogs

- https://bzg.fr/: [[https://bzg.fr/en/blogging-from-emacs.html/][Also in pure Org/Lisp]].
- https://explog.in/: [[https://explog.in/config.html][Also in pure Org/Lisp]]?
- http://jgkamat.gitlab.io/: [[http://jgkamat.gitlab.io/blog/website1.html][Also in pure Org/Lisp]].
- https://ogbe.net/: [[https://ogbe.net/blog/blogging_with_org.html][Also in pure Org/Lisp]].
- https://www.sadiqpk.org/: [[http://sadiqpk.org/blog/2018/08/08/blogging-with-org-mode.html][Also in pure Org/Lisp]].
- http://endlessparentheses.com/: [[http://endlessparentheses.com/how-i-blog-one-year-of-posts-in-a-single-org-file.html][Generated with Jekyll]].
- https://gjhenrique.com/: [[https://gjhenrique.com/meta.html][Generated with Jekyll]].
- http://juanreyero.com/: [[http://juanreyero.com/open/org-jekyll/][Generated with Jekyll]].
- https://babbagefiles.xyz/: [[https://babbagefiles.xyz/new-blog/][Generated with Hugo]].
- http://cestlaz.github.io/: [[http://cestlaz.github.io/posts/using-emacs-35-blogging/][Generated with Nikola]].
